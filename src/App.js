import logo from './logo.svg';
import './App.css';
import { Chart } from "react-google-charts";

const googleData = [
  { type: 'string', label: 'Task ID' },
  { type: 'string', label: 'Task Name' },
  { type: 'string', label: 'Resource' },
  { type: 'date', label: 'Start Date' },
  { type: 'date', label: 'End Date' },
  { type: 'number', label: 'Duration' },
  { type: 'number', label: 'Percent Complete' },
  { type: 'string', label: 'Dependencies' },
]

const googleRowData = [
  [
    '2014Spring',
    'Spring 2014',
    'spring',
    new Date(2014, 2, 22),
    new Date(2014, 5, 20),
    null,
    100,
    null,
  ],
  [
    '2014Summer',
    'Summer 2014',
    'summer',
    new Date(2014, 5, 21),
    new Date(2014, 8, 20),
    null,
    100,
    null,
  ],
  [
    '2014Autumn',
    'Autumn 2014',
    'autumn',
    new Date(2014, 8, 21),
    new Date(2014, 11, 20),
    null,
    100,
    null,
  ],
  [
    '2014Winter',
    'Winter 2014',
    'winter',
    new Date(2014, 11, 21),
    new Date(2015, 2, 21),
    null,
    100,
    null,
  ],
  [
    '2015Spring',
    'Spring 2015',
    'spring',
    new Date(2015, 2, 22),
    new Date(2015, 5, 20),
    null,
    50,
    null,
  ],
  [
    '2015Summer',
    'Summer 2015',
    'summer',
    new Date(2015, 5, 21),
    new Date(2015, 8, 20),
    null,
    0,
    null,
  ],
  [
    '2015Autumn',
    'Autumn 2015',
    'autumn',
    new Date(2015, 8, 21),
    new Date(2015, 11, 20),
    null,
    0,
    null,
  ],
  [
    '2015Winter',
    'Winter 2015',
    'winter',
    new Date(2015, 11, 21),
    new Date(2016, 2, 21),
    null,
    0,
    null,
  ],
  [
    'Football',
    'Football Season',
    'sports',
    new Date(2014, 8, 4),
    new Date(2015, 1, 1),
    null,
    100,
    null,
  ],
  [
    'Baseball',
    'Baseball Season',
    'sports',
    new Date(2015, 2, 31),
    new Date(2015, 9, 20),
    null,
    14,
    null,
  ],
  [
    'Basketball',
    'Basketball Season',
    'sports',
    new Date(2014, 9, 28),
    new Date(2015, 5, 20),
    null,
    86,
    null,
  ],
  [
    'Hockey',
    'Hockey Season',
    'sports',
    new Date(2014, 9, 8),
    new Date(2015, 5, 21),
    null,
    89,
    null,
  ],
]


function App() {
  return (
    <div className="App">
      <Chart
        width={'100%'}
        height={'400px'}
        chartType="Gantt"
        loader={<div>Loading Chart</div>}
        data={[googleData, ...googleRowData]}
        chartEvents={[
          {
            eventName: "select",
            callback: ({ chartWrapper, google }) => {
              function selectHandler(e) {
                console.log(e)
                var selections = chartWrapper.getChart().getSelection();
                console.log(selections)
                if (selections.length == 0) {
                  alert('Nothing selected');
                } else {
                  var selection = selections[0];
                  console.info(selection);
                  alert('You selected ' + selection + ' ' + 
                    (selection.row == null ? 'something' : 'row: ' + selection.row + ' = '));
                }
              };
              google.visualization.events.addListener(chartWrapper.getChart(), 'select', selectHandler); 	
            }
          }
        ]}
        options={{
          title: "Company Performance",
          curveType: "function",
          legend: { position: "bottom" },
          enableInteractivity: true,
          tooltip: { isHtml: true, trigger: "selection" },
          bar: { groupWidth: '95%' },
          height: 1500,
          gantt: {
            trackHeight: 100,
            barHeight: 80,
          },
        }}
        rootProps={{ 'data-testid': '2' }}
      />
    </div>
  );
}

export default App;
